package model.vo;

/**
 * Representation of a route object
 */
public class VORoute {
	private String routeId;
	private String agencyId;
	private String routeShortName;
	private String routeLongName;
	private int routeType;
	private String routeUrl;

	public VORoute (String pRouteId, String pAgencyId, String pRouteSName, String pRouteLName, int pRouteType, String pRouteUrl)
	{
		setRouteId(pRouteId);
		setAgencyId(pAgencyId);
		setRouteShortName(pRouteSName);
		setRouteLongName(pRouteLName);
		setRouteType(pRouteType);
		setRouteUrl(pRouteUrl);

	}
	/**
	 * @return id - Route's id number
	 */
	public int id() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * @return the routeId
	 */
	public String getRouteId() {
		return routeId;
	}
	/**
	 * @param routeId the routeId to set
	 */
	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}
	/**
	 * @return the agencyId
	 */
	public String getAgencyId() {
		return agencyId;
	}
	/**
	 * @param agencyId the agencyId to set
	 */
	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}
	/**
	 * @return the routeShortName
	 */
	public String getRouteShortName() {
		return routeShortName;
	}
	/**
	 * @param routeShortName the routeShortName to set
	 */
	public void setRouteShortName(String routeShortName) {
		this.routeShortName = routeShortName;
	}
	/**
	 * @return the routeLongName
	 */
	public String getRouteLongName() {
		return routeLongName;
	}
	/**
	 * @param routeLongName the routeLongName to set
	 */
	public void setRouteLongName(String routeLongName) {
		this.routeLongName = routeLongName;
	}
	/**
	 * @return the routeType
	 */
	public int getRouteType() {
		return routeType;
	}
	/**
	 * @param routeType the routeType to set
	 */
	public void setRouteType(int routeType) {
		this.routeType = routeType;
	}
	/**
	 * @return the routeUrl
	 */
	public String getRouteUrl() {
		return routeUrl;
	}
	/**
	 * @param routeUrl the routeUrl to set
	 */
	public void setRouteUrl(String routeUrl) {
		this.routeUrl = routeUrl;
	}

}
