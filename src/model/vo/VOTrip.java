package model.vo;

public class VOTrip {

	private String routeId;
	private int serviceId;
	private String tripId;
	private String tripHeadsign;
	private int directionId;
	private int blockId;
	private int shapeId;
	private boolean wheelchairAccessible;
	
	public VOTrip(String routeId, int serviceId, String tripId, String tripHeadsign, int directionId, int blockId, int shapeId, boolean wheelchairAccessible )
	{
		this.routeId=routeId;
		this.serviceId=serviceId;
		this.tripId=tripId;
		this.tripHeadsign=tripHeadsign;
		this.directionId= directionId;
		this.blockId=blockId;
		this.shapeId=shapeId;
		this.wheelchairAccessible=wheelchairAccessible;
		
		
	}

	/**
	 * @return the routeId
	 */
	public String getRouteId() {
		return routeId;
	}

	/**
	 * @param routeId the routeId to set
	 */
	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	/**
	 * @return the serviceId
	 */
	public int getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * @return the tripId
	 */
	public String getTripId() {
		return tripId;
	}

	/**
	 * @param tripId the tripId to set
	 */
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}

	/**
	 * @return the directionId
	 */
	public int getDirectionId() {
		return directionId;
	}

	/**
	 * @param directionId the directionId to set
	 */
	public void setDirectionId(int directionId) {
		this.directionId = directionId;
	}

	/**
	 * @return the tripHeadsign
	 */
	public String getTripHeadsign() {
		return tripHeadsign;
	}

	/**
	 * @param tripHeadsign the tripHeadsign to set
	 */
	public void setTripHeadsign(String tripHeadsign) {
		this.tripHeadsign = tripHeadsign;
	}

	/**
	 * @return the blockId
	 */
	public int getBlockId() {
		return blockId;
	}

	/**
	 * @param blockId the blockId to set
	 */
	public void setBlockId(int blockId) {
		this.blockId = blockId;
	}

	/**
	 * @return the shapeId
	 */
	public int getShapeId() {
		return shapeId;
	}

	/**
	 * @param shapeId the shapeId to set
	 */
	public void setShapeId(int shapeId) {
		this.shapeId = shapeId;
	}

	/**
	 * @return the wheelchairAccessible
	 */
	public boolean isWheelchairAccessible() {
		return wheelchairAccessible;
	}

	/**
	 * @param wheelchairAccessible the wheelchairAccessible to set
	 */
	public void setWheelchairAccessible(boolean wheelchairAccessible) {
		this.wheelchairAccessible = wheelchairAccessible;
	}
	
}
