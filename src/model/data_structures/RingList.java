package model.data_structures;

import java.util.Iterator;
import model.data_structures.Node;


public class RingList<T> implements IList<T>{
	
	private Node<T> head;
	private int size;
	
	public RingList()
	{
		clear();
	}
	
	public void clear()
	{
		head = new Node<T>( null, null, null ); 
		head.changeNext(head);
		head.changePrev(head);
		size = 0; 
	}

	@Override
	public Iterator<T> iterator() {
		return new ListIterator();
	}
	
	private class ListIterator implements Iterator <T>{
		private Node<T> current = head.getNext();
		public boolean hasNext() {
			return current != head;
		}
		public void remove() {
			RingList.this.remove( current.getPrev() );
		}
		public T next(){
			if( !hasNext() ) 
				throw new java.util.NoSuchElementException(); 
			
			T t= (T) current.getData();
			current = current.getNext();
			return t;
		}
	}
	
	public void add( T x )
	{
		add( size, x );
	}
	public void add( int idx, T x )
	{
		addBefore( getNode( idx, 0, size ), x );
	}
	private void addBefore( Node<T> p, T x )
	{
		Node<T> newNode = new Node<T>( x, p.getPrev(), p ); 
		newNode.getPrev().changeNext(newNode);
		p.changePrev(newNode); 
		size++ ;
	} 
	
	public T remove( int idx )
	{
		return remove( getNode( idx, 0, size ) );
	}
	
	private T remove( Node<T> p )
	{
		p.getNext().changePrev(p.getPrev());
		p.getPrev().changeNext(p.getNext());
		p.changeNext(null);
		p.changePrev(null); 
		size-- ;
		return p.getData();
	} 
	
	private Node<T> getNode( int idx, int lower, int upper )
	{
		Node<T> p;
		p = head.getNext();
		if(size !=0){
		for ( int i = 0; i < idx%size; i++ )
		p = p.getNext(); 
		}
		return p;

	} 

	@Override
	public void addFirst(T t) {
		add(0, t);
	}

	@Override
	public void addAtEnd(T t) {
		add(size, t);
	}

	@Override
	public void addAdK(T t, int pos) {
		add(pos, t);
	}

	@Override
	public T getElement(int pos) {
		Node<T> buscado = getNode(pos, 0, size);
		return buscado.getData();
	}

	@Override
	public Integer getSize() {
		Integer rta = new Integer(size);
		return rta;
	}

	@Override
	public void deleteFirst() {
		remove(0);
	}

	@Override
	public void deleteLast() {
		remove(size);
	}

	@Override
	public void deleteAtK(int pos) {
		remove(pos);
	}
	
	
	
	@Override
	public T getCurrentElement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T previous() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
