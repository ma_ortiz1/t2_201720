package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.swing.plaf.synth.SynthSpinnerUI;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.RingList;

public class STSManager implements ISTSManager 
{

	private  IList<VORoute> listRoutes; 
	private IList<Zone> listStops;
	private IList<VOTrip> listTrips;
	private IList<VOStopTime> listStopsTimes;
	private IList<VOStop> listRStops;


	public class VOStopTime{
		private String tripId;
		private String arrivalTime;
		private String departureTime;
		private String stopSequence;
		private String stopId;
		private int stopHeadsign;
		public VOStopTime(String tripId, String arrivalTime, String departureTime,String stopSequence,int stopHeadsign){
			this.tripId=tripId;
			this.arrivalTime=arrivalTime;
			this.departureTime=departureTime;
			this.stopSequence=stopSequence;
			this.stopHeadsign=stopHeadsign;
		}
		/**
		 * @return the tripId
		 */
		public String getTripId() {
			return tripId;
		}
		/**
		 * @param tripId the tripId to set
		 */
		public void setTripId(String tripId) {
			this.tripId = tripId;
		}
		/**
		 * @return the arrivalTime
		 */
		public String getArrivalTime() {
			return arrivalTime;
		}
		/**
		 * @param arrivalTime the arrivalTime to set
		 */
		public void setArrivalTime(String arrivalTime) {
			this.arrivalTime = arrivalTime;
		}
		/**
		 * @return the departureTime
		 */
		public String getDepartureTime() {
			return departureTime;
		}
		/**
		 * @param departureTime the departureTime to set
		 */
		public void setDepartureTime(String departureTime) {
			this.departureTime = departureTime;
		}
		/**
		 * @return the stopSequence
		 */
		public String getStopSequence() {
			return stopSequence;
		}
		/**
		 * @param stopSequence the stopSequence to set
		 */
		public void setStopSequence(String stopSequence) {
			this.stopSequence = stopSequence;
		}
		/**
		 * @return the stopHeadsign
		 */
		public int getStopHeadsign() {
			return stopHeadsign;
		}
		/**
		 * @param stopHeadsign the stopHeadsign to set
		 */
		public void setStopHeadsign(int stopHeadsign) {
			this.stopHeadsign = stopHeadsign;
		}
		/**
		 * @return the stopId
		 */
		public String getStopId() {
			return stopId;
		}
		/**
		 * @param stopId the stopId to set
		 */
		public void setStopId(String stopId) {
			this.stopId = stopId;
		}


	}

	public class Zone {
		public String zoneId;
		public DoubleLinkedList<VOStop> listStops;


		public Zone(String zoneId)
		{
			this.zoneId=zoneId;
			this.listStops=new DoubleLinkedList<VOStop>();
		}
		public DoubleLinkedList<VOStop> getListStops(){
			return listStops;
		}
		public String getZoneId(){
			return zoneId;
		}
		public void setZoneId(String zoneId){
			this.zoneId=zoneId;
		}
		//

		public void addListStops(VOStop stop){
			VOStop head= listStops.first();
			VOStop last=listStops.last();


			if (head==null || head.getStopId().compareToIgnoreCase(stop.getStopId())<0){
				listStops.add(stop);

			}
			else if(last!=null && last.getStopId().compareToIgnoreCase(stop.getStopId())<0){
				listStops.addAtEnd(stop);

			}		
			else 
			{
				int size= listStops.getSize();
				int i= 0;
				int pos=0;
				VOStop ac=listStops.getElement(i);
				while(i<size)
				{
					ac=listStops.getElement(i);
					if(ac.getStopId().compareToIgnoreCase(stop.getStopId())>0)
					{
						pos=i;

					}						
					i++;
				}

				listStops.addAdK(stop, pos);								

			}
		}


	}



	@Override
	public void loadRoutes(String routesFile) {

		listRoutes = new DoubleLinkedList<VORoute>();
		File f= new File(routesFile);
		FileReader reader;
		try
		{ 
			if (f.exists())
			{
				reader = new FileReader(f);
				BufferedReader lector = new BufferedReader(reader);
				String line =lector.readLine();
				line =lector.readLine();
				while(line != null)
				{

					String []routes = line.split(",");
					String routeId= routes[0];
					String agencyId= routes[1];
					String pRouteSName=routes[2];
					String pRouteLName=routes[3];
					int pRouteType=Integer.parseInt(routes[5]);
					String pRouteUrl=routes[6];			

					VORoute ruta = new VORoute(routeId, agencyId, pRouteSName, pRouteLName, pRouteType, pRouteUrl);
					listRoutes.addAtEnd(ruta);

					line =lector.readLine();
					System.out.println(ruta.getRouteLongName());
				}
				lector.close();
				reader.close();
				System.out.println(listRoutes.getSize());
			}


		}
		catch (Exception e)
		{

		}

		// TODO Auto-generated method stub

	}


	@Override
	public void loadTrips(String tripsFile) {
		// TODO Auto-generated method stub
		listTrips= new RingList<VOTrip>();
		File f=new File(tripsFile);
		FileReader reader;
		try {
			if(f.exists()){
				reader = new FileReader(f);
				BufferedReader lector = new BufferedReader(reader);
				String line=lector.readLine();
				line=lector.readLine();
				while (line !=null){

					String []trips=line.split(",");
					String routeId=trips[0];
					int serviceId=Integer.parseInt(trips[1]);
					String tripId= trips[2];
					String tripHeadsign=trips[3];
					int directionId=Integer.parseInt(trips[5]);
					int blockId=Integer.parseInt(trips[6]);
					int shapeId=Integer.parseInt(trips[7]);
					int wheAcc= Integer.parseInt(trips[8]);
					boolean wheelchairAccessible;
					wheelchairAccessible = wheAcc > 0 ? true : false ;

					VOTrip trip=new VOTrip(routeId, serviceId, tripId, tripHeadsign, directionId, blockId, shapeId, wheelchairAccessible); 
					listTrips.addAtEnd(trip);
					line=lector.readLine();
					
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}


	}


	@Override
	public void loadStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub
		listStopsTimes  = new DoubleLinkedList<VOStopTime>();
		File f= new File(stopTimesFile);
		FileReader reader;
		try{
			if(f.exists())
			{
				reader = new FileReader(f);
				BufferedReader lector = new BufferedReader(reader);
				String line=lector.readLine();
				line=lector.readLine();
				int j=0;
				while(line!=null){

					String []timeStop=line.split(",");
					String tripId= timeStop[0];
					String arrivalTime= timeStop[1];
					String departureTime=timeStop[2];
					String stopId=timeStop[3];
					String stopSequence=timeStop[4];
					int stopHeadsign=Integer.parseInt(timeStop[6]);
					VOStopTime stopTime=new VOStopTime(tripId, arrivalTime, departureTime, stopSequence, stopHeadsign);
					listStopsTimes.addAtEnd(stopTime);
					j++;
					line=lector.readLine();			
				}
				lector.close();
				reader.close();
			}

		}catch(Exception e){
			e.printStackTrace();
		}

	}

	@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		listStops = new RingList<Zone>();

		File f= new File(stopsFile);
		FileReader reader; 
		try 
		{
			if (f.exists())
			{
				reader = new FileReader(f);
				BufferedReader lector = new BufferedReader(reader);
				String line = lector.readLine();
				line=lector.readLine();

				while(line != null )
				{


					String []stops = line.split(",");

					String stopId=stops[0];

					String stopName= stops[2];
					String stopDesc=stops[3];					
					double  stopLat=Double.parseDouble(stops[4]);
					double  stopLon=Double.parseDouble(stops[5]);
					String zoneId=stops[6];
					int locationType=Integer.parseInt(stops[8]);
					VOStop stop=new VOStop(stopId, stopName, stopDesc, stopLat, stopLon, zoneId, locationType);
					int i =findZone(zoneId);
					if (i != -1){

						listStops.getElement(i).addListStops(stop);
					}else{
						Zone one= new Zone(zoneId);
						one.addListStops(stop);
						if(listStops.getSize()==0)
						{
							listStops.addFirst(one);
						}
						else
						{						
							listStops.addAtEnd(one);
						}
					}

					line=lector.readLine();			
				}
				lector.close();
				reader.close();


			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public int findZone(String zoneId){
		int i=-1;
		int j=0;
		while(j<listStops.getSize() ){
			Zone curre=listStops.getElement(j);
			if(zoneId.equals(curre.getZoneId())){
				i=j;
				break;
			}
			j++;
		}
		return i;
	}
	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		DoubleLinkedList<VORoute> list=new DoubleLinkedList<VORoute>();
		int i =0;
		while(i<listTrips.getSize()){
			VOTrip r=listTrips.getElement(i);
			String tripId=r.getTripId();
			String routeId=r.getRouteId();
			VORoute ro=null;
			for (int k=0; k<listRoutes.getSize();k++){
				ro=listRoutes.getElement(k);
				if(ro.getRouteId().equals(routeId)){
					ro=listRoutes.getElement(k);
					k=listRoutes.getSize();
				}
			}
			for (int j=0; j<listStopsTimes.getSize();j++){
				VOStopTime s=listStopsTimes.getElement(j);
				if(tripId.equals(s.getTripId())){
					if(stopName.equals(s.getStopId())){
						list.add(ro);
					}
				}
			}
			i++;

		}
		return list;
	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		return null;
	}

}
